> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381: Mobile Web Application Development

## Marielle Estrugo

### Assignment 2 Requirements:

#Screenshot of First Interface
![Interface 1](img/screen_1.png)

#Screenshot of Second Interface
![interface 2](img/screen_2.png)

# Questions - Chapters 3 and 4:

1. You use the ___ statement to retrieve records from a table.
	* SELECT
2. The ____ keyword specifies the name of the table that needs to be modified when
updating records.
	* UPDATE
3. To perform a reverse sort on query results from a SELECT statement, add the ___
keyword after the name of the field by which you want to perform the sort.
	* DESC
4. To store text in an address field, you specify a data type of ____ because the
amount of storage space allocated will vary depending upon the number of
characters in the field.
	* VARCHAR
5. You can arrange query results from a SELECT statement using the keyword(s) ____.
	* SORT BY
6. You can specify which records to return from a database by using the ____ clause.
	* WHERE
7. A join that returns records from related tables only if their related fields match is
called
	* an inner join
	`SELECT vendorName, invoiceNumber, invoiceDate, invoiceTotal`
	`FROM vendors Inner Join invoices`
 		`On vendors.vendorID = invoices.vendorID`
		`WHERE invoiceTotal >= 500`
		`ORDER BY vendorName DESC`
8.	If vendorName contains string data and invoiceTotal contains decimal values, how
will the result set be ordered?
	* alphabetically starting with Z
```sql
SELECT vendorName, invoiceNumber, invoiceDate, invoiceTotal
FROM vendors Inner Join invoices
 On vendors.vendorID = invoices.vendorID
WHERE invoiceTotal >= 500
ORDER BY vendorName DESC
```
9.	How many columns will the above result set have?
	* 4
SELECT vendorName, invoiceNumber, invoiceDate, invoiceTotal
FROM vendors Inner Join invoices
 On vendors.vendorID = invoices.vendorID
WHERE invoiceTotal >= 500
ORDER BY vendorName DESC
10.	What table(s) does the data in the result set above come from?
	* vendors and invoices
11.	When you code an INSERT statement, you don’t have to include the data for a
column that
a. is a foreign key b. is a primary key c. has a default value d. does not allow null values
12.	When you code a DELETE or an UPDATE statement, you usually need to include
a. a SORT BY clause b. a WHERE clause c. an inner join d. an outer join
13.	If a row in one table is related to just one row in another table, the tables are said to
have a
a. one-to-many relationship
b. one-to-one relationship
c. many-to-many relationship
d. unary relationship
14.	The column definition for a MySQL table can be used to determine all but one of the
following. Which one is it?
a. what type of data the column can contain
b. whether the column can contain a null value
c. whether the column has a default value
d. what range of values the column can contain
15. The result set retrieved by the following SELECT statement contains rows that have
SELECT balance, number
FROM accounts
WHERE balance < 0
a. all of the columns from the accounts table
b. two of the rows from the account table
c. all of the columns from the accounts table where balance is less than 0
d. two of the columns from the accounts table where balance is less than 0
16. What does a relational database use in the child tables to relate to the primary keys
in the parent tables?
a. indexes b. foreign keys c. non-primary keys d. primary keys
17. What does a relational database use to uniquely identify each row in a table?
a. indexes b. foreign keys c. non-primary keys d. primary keys
18. Which of the following can a SELECT statement not do to the data in a table?
a. Get selected rows b. List selected columns c. Sort the rows d. Delete the rows
6
The starting code for the index.php file which is the first page of an application
```php
<?php
 require 'database.php';
 $category_id = $_GET['category_id'];
 if (!isset($category_id)) {
 $category_id = 1;
 }
 // Routine 1
 $query = "SELECT * FROM categories
 WHERE categoryID = $category_id";
 $category = $db->query($query);
 $category = $category->fetch();
 $category_name = $category['categoryName'];
 // Routine 2
 $query = "SELECT * FROM products
 WHERE categoryID = $category_id
 ORDER BY productID";
 $products = $db->query($query);
?>
```
19. The first statement in this example gets and runs a file named database.php. What
must this code do for the rest of the statements in this example to work?
a. Create a PDOStatement object named $db that connects to the right database
b. Create a PDOStatement object named $db-> that connects to the right database
c. Create a PDO object named $db that connects to the right database
d. Create a PDO object named $db-> that connects to the right database
The starting code for the index.php file which is the first page of an application
```php
<?php
 require 'database.php';
 $category_id = $_GET['category_id'];
 if (!isset($category_id)) {
 $category_id = 1;
 }
 // Routine 1
 $query = "SELECT * FROM categories
 WHERE categoryID = $category_id";
 $category = $db->query($query);
 $category = $category->fetch();
 $category_name = $category['categoryName'];
 // Routine 2
 $query = "SELECT * FROM products
 WHERE categoryID = $category_id
 ORDER BY productID";
 $products = $db->query($query);
?>
```
20. What does routine 1 above store in the variable named $category_name?
a. The category name for the first row in the categories table of the database
b. The category name for the row in categories table that corresponds to the value in
$category_id
c. The category name for the row in categories table that has a category ID of 1
d. An array of the category names in the categories table
The starting code for the index.php file which is the first page of an application
```php
<?php
 require 'database.php';
 $category_id = $_GET['category_id'];
 if (!isset($category_id)) {
 $category_id = 1;
 }
 // Routine 1
 $query = "SELECT * FROM categories
 WHERE categoryID = $category_id";
 $category = $db->query($query);
 $category = $category->fetch();
 $category_name = $category['categoryName'];
 // Routine 2
 $query = "SELECT * FROM products
 WHERE categoryID = $category_id
 ORDER BY productID";
 $products = $db->query($query);
?>
```
21. What does routine 2 store in the variable named $products?
a. A PDOStatement object for all rows in the products table
b. A PDOStatement object for the columns in the first row in the products table
c. A PDOStatement object for the rows in the products table that have a category ID equal
to the value in $category_id
d. A PDOStatement object for the rows in the products table that have a category ID equal
to 1
22. In the catch block of a try/catch statement for handling PDO exceptions, you can get
a message that describes the exception by using the getMessage method of the
a. PDO object b. PDOStatement object c. PDOException object d. Result set array
23. Which of the following is the correct way to code a PHP statement that puts the first
row of PDOStatement object named $products in an array named $product?
a. `$product = $db->query($products);`
b. `$product = $db->fetch($products);`
c. `$product = $products->query();`
d. `$product = $products->fetch();`
24. Which of the following is the correct way to code a PHP statement that returns the
result set for a SELECT statement that’s stored in $statement if the PDO object is
$db?
a. `$results = $db->query($statement);`
b. `$results = $db->exec($statement);`
c. `$results = query->$db($statement);`
d. `$results = exec->$db($statement);`
25. Which of the following is the correct way to code a PHP statement that executes an
INSERT, UPDATE, or DELETE statement that’s stored in $statement if the PDO object
is $db?
a. `$modify_count = $db->query($statement);`
b. `$modify_count = $db->exec($statement);`
c. `$modify_count = query->$db($statement);`
d. `$modify_count = exec->$db($statement);`
26. When you create a PDO object, you have to pass all but one of these arguments to
it: Which one is it?
a. Data source name b. Server name c. User name d. Password