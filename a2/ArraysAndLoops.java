import java.util.Scanner;

class ArraysAndLoops
{
	public static void main(String args [])
	{
		//display operational messages
		System.out.println("Program loops through array of strings.");
		System.out.println("Use the following values: dog, cat, bird, fish, insect.");
		System.out.println("Use the following loop structures: for, enhanced for, while, do... while.");
		System.out.println("");
		System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do ... while.");

		//java style string[] animals
		String animals[] = {"dog", "cat", "bird", "fish", "insect"};

		System.out.println("for loop: ");
		for(int i = 0; i < animals.length; i++)
		{
			System.out.println(animals[i]);
		}

		System.out.println("\nEnhanced for loop: ");
		for(String test: animals)
		{
			System.out.println(test);
		}

		System.out.println("\nWhile loop:");
		int i=0;
		while (i < animals.length) {
			System.out.println(animals[i]);
			i++;
		}

		System.out.println("\ndo-while loop:");
		i = 0;
		do {
			System.out.println(animals[i]);
			i++;
		}while (i < animals.length);
	}
}