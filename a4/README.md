> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381: Mobile Web Application Development

## Marielle Estrugo

### Assign 4 Requirements:

* Regex Validation

##Validation
![Home Page](img/screen_4.jpeg)
![Invalid 1](img/screen_2.jpeg)
![Invalid 2](img/screen_3.jpeg)
![Validation](img/screen_1.jpeg)


##Questions:
![Questions Part 1](img/q_1.jpg)
![Questions Part 2](img/q_2.jpg)
![Questions Part 3](img/q_3.jpg)
![Questions Part 4](img/q_4.jpg)
![Questions Part 5](img/q_5.jpg)
![Questions Part 6](img/q_6.jpg)