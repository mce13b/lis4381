<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//Get item data
//no need for str_id when adding, uses auto increment
$str_id_v = $_POST['str_id'];
$str_name_v = $_POST['str_name'];
$str_street_v = $_POST['str_street'];
$str_city_v = $_POST['str_city'];
$str_state_v = $_POST['str_state'];
$str_zip_v = $_POST['str_zip'];
$str_phone_v = $_POST['str_phone'];
$str_email_v = $_POST['str_email'];
$str_url_v = $_POST['str_url'];
$str_ytd_sales_v = $_POST['str_ytd_sales'];
$str_notes_v = $_POST['str_notes'];

//See Ch15: Regular Expressions
//name: letters, numbers, hyphens, and underscore
$pattern='/^[a-zA-Z0-9\-_\s]+$/';
$valid_name = preg_match($pattern, $str_name_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//street: letters, numbers, hyphens, and underscore
$pattern='/^[a-zA-Z0-9,\s\.]+$/';
$valid_street = preg_match($pattern, $str_street_v);
//echo $valid_street; //test output: should be 1 (i.e., valid)
//exit();

//city: letters, numbers, hyphens, and underscore
$pattern='/^[a-zA-Z\s]+$/';
$valid_city = preg_match($pattern, $str_city_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//state: letters, numbers, hyphens, and underscore
$pattern='/^[a-zA-Z]{2}+$/';
$valid_state = preg_match($pattern, $str_state_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//zip: letters, numbers, hyphens, and underscore
$pattern='/^\d{5,9}+$/';
$valid_zip = preg_match($pattern, $str_zip_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//phone: letters, numbers, hyphens, and underscore
$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $str_phone_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//email: letters, numbers, hyphens, and underscore
$pattern='/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email = preg_match($pattern, $str_email_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//URL: letters, numbers, hyphens, and underscore
$pattern='/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
$valid_url = preg_match($pattern, $str_url_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

//ytd_sales: letters, numbers, hyphens, and underscore
$pattern='/^\d{1,8}(?:\.\d{0,2})?$/';
$valid_ytd_sales = preg_match($pattern, $str_ytd_sales_v);
//echo $valid_name; //test output: should be 1 (i.e., valid)
//exit();

// validate inputs - must contain all required fields
if
(
	empty($str_name_v) ||
	empty($str_street_v) ||
	empty($str_city_v) ||
	empty($str_state_v) ||
	empty($str_zip_v) ||
	empty($str_phone_v) ||
	empty($str_email_v) ||
	empty($str_url_v) ||
	!isset($str_ytd_sales_v)
)
{
	$error = "All fields require data, except <b>Notes</b>. Check all fields and try again.";
	include('global/error.php');
}

//YTD Sales: must contain numbers and be equal to or greater than 0
else if (!is_numeric($str_ytd_sales_v) || $str_ytd_sales_v <= 0)
{
	$error = 'YTD Sales can only contain numbers (other than a decimal point); and must be equal to or greater than zero.';
	include('global/error.php');
}

else if ($valid_name === false)
{
	echo 'Error in pattern!';
}

else if ($valid_name === 0)
{
	$error = 'Name can only contain letters, numbers, hypens, and underscore.';
	include('global/error.php');
}

else if ($valid_street === false)
{
	echo 'Error in pattern!';
}

else if ($valid_street === 0)
{
	$error = 'Street can only contain letters, numbers, commas, and periods.';
	include('global/error.php');
}

else if ($valid_city === false)
{
	echo 'Error in pattern!';
}

else if ($valid_city === 0)
{
	$error = 'City can only contain letters.';
	include('global/error.php');
}

else if ($valid_state === false)
{
	echo 'Error in pattern!';
}

else if ($valid_state === 0)
{
	$error = 'State must contain two letters.';
	include('global/error.php');
}

else if ($valid_zip === false)
{
	echo 'Error in pattern!';
}

else if ($valid_zip === 0)
{
	$error = 'Zip must contain 5-9 digits, and no other characters.';
	include('global/error.php');
}

else if ($valid_phone === false)
{
	echo 'Error in pattern!';
}

else if ($valid_phone === 0)
{
	$error = 'Phone must contain 10 digits, and no other characters.';
	include('global/error.php');
}

else if ($valid_email === false)
{
	echo 'Error in pattern!';
}

else if ($valid_email === 0)
{
	$error = 'Email must contain a valid email address.';
	include('global/error.php');
}

else if ($valid_url === false)
{
	echo 'Error in pattern!';
}

else if ($valid_url === 0)
{
	$error = 'URL must contain a valid URL.';
	include('global/error.php');
}

else if ($valid_ytd_sales === false)
{
	echo 'Error in pattern!';
}

else if ($valid_ytd_sales === 0)
{
	$error = 'YTD_Sales must contain no more than 10 digits, including a decimal point.';
	include('global/error.php');
}

else
{
	require_once('global/connection.php');
	$query =
	"UPDATE store
	SET
		str_name = :str_name_p,
		str_street = :str_street_p,
		str_city = :str_city_p,
		str_state = :str_state_p,
		str_zip = :str_zip_p,
		str_phone = :str_phone_p,
		str_email = :str_email_p,
		str_url = :str_url_p,
		str_ytd_sales = :str_ytd_sales_p,
		str_notes = :str_notes_p
	WHERE str_id = :str_id_p";
	
	//exit($query);
	
		try
		{
			$statement = $db->prepare($query);
			$statement->bindParam(':str_id_p', $str_id_v);
			$statement->bindParam(':str_name_p', $str_name_v);
			$statement->bindParam(':str_street_p', $str_street_v);
			$statement->bindParam(':str_city_p', $str_city_v);
			$statement->bindParam(':str_state_p', $str_state_v);
			$statement->bindParam(':str_zip_p', $str_zip_v);
			$statement->bindParam(':str_phone_p', $str_phone_v);
			$statement->bindParam(':str_email_p', $str_email_v);
			$statement->bindParam(':str_url_p', $str_url_v);
			$statement->bindParam(':str_ytd_sales_p', $str_ytd_sales_v);
			$statement->bindParam(':str_notes_p', $str_notes_v);
			$row_count = $statement->execute();
			$statement->closeCursor();
			
			
			
			
		}
		
		catch (PDOException $e)
		{
			$error = $e->getMessage();
			echo $error;
		}
	header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)	
}
?>