<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__).'/error_log.txt');
error_reporting(E_ALL);

// Get item ID
$str_id_v = $_POST['str_id'];

require_once('global/connection.php');

//Delete item from database
$query =
"DELETE FROM store
WHERE str_id = :str_id_p";

try
{
	$statement = $db->prepare($query);
	$statement->bindParam(':str_id_p', $str_id_v);
	$row_count = $statement->execute();
	$statement->closeCursor();
	
	//view rows affected, comment when done testing
	//exit($row_count);
	
	//include('index.php'); //forwarding is faster, once trip to server
	header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
	}
	
catch (PDOException $e)
{
	$error = $e->getMessage();
	echo $error;
}	
?>