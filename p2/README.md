> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381: Mobile Web Application Development

## Marielle Estrugo

### Project 2 Requirements:

* Edit/Delete Button
* RSS Feed
* Home page Carousel

##Edit/Delete
![index](img/indexphp.jpeg)
![Edit](img/edit.jpeg)
![Error](img/edit_error.jpeg)

##Home
![Home](img/home.jpeg)

##RSS Feed
![RSS](img/rss.jpeg)

