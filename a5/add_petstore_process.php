<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//code to process inserts goes here
//Get item id 
$str_name_v=$_POST['name'];
$str_street_v=$_POST['street'];
$str_city_v=$_POST['city'];
$str_state_v=$_POST['state'];
$str_zip_v=$_POST['zip'];
$str_phone_v=$_POST['phone'];
$str_email_v=$_POST['email'];
$str_url_v=$_POST['url'];
$str_ytd_sales_v=$_POST['ytd_sales'];
$str_notes_v=$_POST['notes'];

//exit($str_name_v);

//allows characters, numbers, hyphes, underscores
$pattern="/^[a-zA-Z0-9\-_\s]+$/";
$valid_name=preg_match($pattern, $str_name_v);

$pattern="/^[a-zA-Z0-9,\s\.]+$/";
$valid_street=preg_match($pattern, $str_street_v);

$pattern="/^[a-zA-Z0-9\s]+$/";
$valid_city=preg_match($pattern, $str_city_v);

$pattern="/^[a-zA-Z]{2,2}+$/";
$valid_state=preg_match($pattern, $str_state_v);

$pattern="/^[0-9]{5,9}+$/";
$valid_zip=preg_match($pattern, $str_zip_v);

$pattern="/^[0-9]{10,10}+$/";
$valid_phone=preg_match($pattern, $str_phone_v);

$pattern="/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/";
$valid_email=preg_match($pattern, $str_email_v);

$pattern="/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/";
$valid_url=preg_match($pattern, $str_url_v);

$pattern="/^[0-9\.]+$/";
$valid_ytd_sales=preg_match($pattern, $str_ytd_sales_v);

//empty() function returns true for following values: empty strings (""), zero(0), "0", null, or false
//isset returns false when value is null
if 
(
empty($str_name_v)||
empty($str_street_v)||
empty($str_city_v)||
empty($str_state_v)||
empty($str_zip_v)||
empty($str_phone_v)||
empty($str_email_v)||
empty($str_url_v)||
!isset($str_ytd_sales_v)
)

{
$error="All fields are required, excpet for <b>Notes</b>. Check all fields and try again.";
include('global/error.php');
}
else if(!is_numeric($str_ytd_sales_v) || $str_ytd_sales_v <=0)
{
$error="YTD sales must be numeric and must be greater than or equal to zero";
include('global/error.php');
}
/*
if error in regular expression, preg_match returns false.
if no match of patern in string, preg_match returns 0
*/
else if($valid_name === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_name === 0)
{
$error="Pet Store Name can only contain letters, numbers, commas and periods";
include('global/error.php');
}
else if($valid_street === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_street === 0)
{
$error="Street can only contain letters, numbers, commas and periods";
include('global/error.php');
}
else if($valid_city === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_city === 0)
{
$error="City can only contain letters";
include('global/error.php');
}
else if($valid_state === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_state === 0)
{
$error="State can only contain letters and can't be more than 2 characters";
include('global/error.php');
}
else if($valid_zip === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_zip === 0)
{
$error="Zip can only contain numbers and no more nine digits";
include('global/error.php');
}
else if($valid_phone === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_phone === 0)
{
$error="Phone can only contain digits and must be ten digits";
include('global/error.php');
}
else if($valid_email === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_email === 0)
{
$error="Please enter a valid email";
include('global/error.php');
}
else if($valid_url === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_url === 0)
{
$error="URL can not be more than 100 characters";
include('global/error.php');
}
else if($valid_ytd_sales === false)
{
$error="Error in pattern";
include('global/error.php');
}
else if($valid_ytd_sales === 0)
{
$error="YTD Sales can only contain numbers";
include('global/error.php');
}

else
{
require_once "global/connection.php";
$query =
"INSERT INTO store
(str_name, 
str_street, 
str_city, 
str_state, 
str_zip, 
str_phone, 
str_email, 
str_url, 
str_ytd_sales, 
str_notes)
VALUES
( :str_name_p,
:str_street_p,
:str_city_p,
:str_state_p,
:str_zip_p,
:str_phone_p,
:str_email_p,
:str_url_p,
:str_ytd_sales_p,
:str_notes_p)";
try
{
$statement = $db->prepare($query);
$statement->bindParam(':str_name_p',$str_name_v);
$statement->bindParam(':str_street_p',$str_street_v);
$statement->bindParam(':str_city_p',$str_city_v);
$statement->bindParam(':str_state_p',$str_state_v);
$statement->bindParam(':str_zip_p',$str_zip_v);
$statement->bindParam(':str_phone_p',$str_phone_v);
$statement->bindParam(':str_email_p',$str_email_v);
$statement->bindParam(':str_url_p',$str_url_v);
$statement->bindParam(':str_ytd_sales_p',$str_ytd_sales_v);
$statement->bindParam(':str_notes_p',$str_notes_v);
$statement->execute();
$statement->closeCursor();
$last_auto_increment_id = $db->lastInsertId();
//include('index.php'); //forwarding is faster, one trip to server
header('Location: index.php');
}
catch(PDOEXception$e)
{
$error = $e->getMessage();
echo $error;
}
}
?>