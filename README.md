> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381: Mobile Web Application Development

## Marielle Estrugo

[A1 README.md](a1/README.md "My A1 README.md file")

* Screenshot of AMPPS Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands with short descriptions
* Bitbucket repo links: a) assignment and b) the completed tutorials

[A2 README.md](a2/README.md "My A2 README.md file")

* Screenshot of Activity Screen 1
* Screenshot of Activity Screen 2

[A3 README.md](a3/README.md "My A3 README.md file")

Part 1:

* a3.mwb
* a3.sql
* a3.png

Part 2:

* Screenshot of first interface
* Screenshot of second interface

[Project 1 README.md](p1/README.md "My Project 1 README.md file")

Part 1:

* Launcher Icon
* Background Colors
* Border
* Text Shadow

Part 2:

* Screenshot of first interface
* Screenshot of second interface

[A4 README.md](a4/README.md "My A4 README.md file")

* Regex validation
* Client side validation

[A5 README.md](a5/README.md "My A5 README.md file")

* Regex validation
* Server side validation

[P2 README.md](a5/README.md "My P2 README.md file")

* Edit/Delete Button
* RSS Feed
* Home page Carousel