> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381: Mobile Web Application Development

## Marielle Estrugo

### Project 1 Requirements:

Part 1:

* Launcher Icon
* Background Colors
* Border
* Text Shadow

Part 2:

* Screenshot of first interface
* Screenshot of second interface

##Part 1:

####Link to A3.mwb
[a3.mwb](a3.mwb)

####Screenshot of ERD
![a3.png](img/a3_erd.png)

####Screenshot of Localhost running with ERD
![Web View](img/web.jpg)

##Part 2: .

####App Screen 1
![screen_1.jpg](img/screen_1.jpg)

####App Screen 2
![screen_2.jpg](img/screen_2.jpg)

##Questions:
![Questions Part 1](img/q1.jpg)
![Questions Part 2](img/q2.jpg)
![Questions Part 3](img/q3.jpg)