> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381: Mobile Web Application Development

## Marielle Estrugo

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Instillations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands with short descriptions
* Bitbucket repo links: a) assignment and b) the completed tutorials

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Git commands w/short descriptions:
![Git from previous assignment](img/git.png)

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

#### Part 3:
*Chapter 1 and 2 Questions*

1. You create line comments in PHP code by adding to a line you want to use as a comment. (Choose all that apply.)
> b. **
> d. //
2. Block comments begin with /* and end with _____.
> b. */
3. PHP variables begin with _____________________.
> c. $
4. A variable name in PHP can only contain letters, numbers, and
_______________________.
> a. underscores
5. A PHP variable name
> a. is case-sensitive
6. A web application is a type of
> a. client/server application
7. After the if statement that follows is executed, what will the value of $discountAmount
be?
	```
	$discount_amount;
	$order_total = 200;
	if ($order_total > 200) {
	 $discount_amount = $order_total * .3;
	} else if ($order_total > 100) {
	 $discount_amount = $order_total * .2;
	} else {
	 $discount_amount = $order_total * .1;
	}
	```
> c. 40
8. How many times will the while loop that follows be executed?
	```
	$months = 5;
	$i = 1;
	while ($i > $months) {
	 $futureValue = $futureValue * (1 + $monthlyInterestRate);
	 $i = $i+1;
	}
	```
> a. 0
9. If a URL specifies a directory that doesn’t contain a default page, Apache displays
> d. a list of all of the directories in that directory
10. In the code that follows, if $error_message isn’t empty
	```
	 if ($error_message != '') {
	 include('index.php');
	 exit();
	 }
	```
> a. control is passed to a page named index.php that’s in the current directory
11. The HTTP response for a dynamic web page is passed
> b. from the web server to the browser
12. The order of precedence for arithmetic expressions causes
> c. increment operations to be done first
13. To round and format the value of a variable named $number to 3 decimal places and
store it in a variable named $number_formatted, you code
> b. $number_formatted = number_format($number, 3);
14. To run a PHP application that has been deployed on your own computer, you can enter a
URL in the address bar of your browser that
> b. uses localhost as the domain name
15. To view the source code for a web page in the Firefox or IE browser, you can select the appropriate command from the
> b. Source menu
16. When the web server recieves an HTTP request for a PHP page, the web server calls the
> d. PHP interpreter
17. Which of the following is NOT part of an HTTP URL:
> c. node
18. What will the variable $name contain after the code that follows is executed?
	```
	$first_name = 'Bob';
	$last_name = 'Roberts';
	$name = "Name: $first_name";
	```
> c. Name: Bob
19. In PHP, the concatenation operator is
> c. .
20. In a conditional expression that includes logical operators, the AND operator has a lower order of precedence than the ______________ operator.
> b. OR
21. A(n) ____ operator requires a single operand either before or after the operator.
> a. unary
22. Assignment operators are used to assign a ____ to a variable.
> b. value
23. ____ a variable is the processing of assigning a first value to a variable at the time of creation.
> a. initializing